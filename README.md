https://www.w3schools.com/html/html_intro.asp Copied HTML page structure
https://www.w3schools.com/tags/tag_meta.asp copied and pasted meta tags 
https://www.w3schools.com/tags/tag_link.asp copied the link tag to connect css to html
https://www.javatpoint.com/how-to-center-images-in-css help center the logo and button
https://dev.to/nehalahmadkhan/how-to-make-footer-stick-to-bottom-of-web-page-3i14 copied code to get the footer to stick to the bottom of the page
https://www.w3schools.com/cssref/css3_pr_box-shadow.asp used code to place a drop shadow on buttons 
https://www.w3schools.com/cssref/pr_font_weight.asp to refresher on adjusting font weight
https://stackoverflow.com/questions/6654958/make-body-have-100-of-the-browser-height copied code to get the footer to stick to the bottom of the page
https://www.google.com/search?q=how+to+keep+gradient+from+repeating&oq=how+to+keep+gradient+from+repeating+&aqs=chrome..69i57j0i22i30.16082j0j7&sourceid=chrome&ie=UTF-8 keep gradient from repeating on home page
https://www.rapidtables.com/convert/color/hex-to-rgb.html find the conversion of hex values to rgb to create the gradient
https://www.w3schools.com/css/tryit.asp?filename=trycss3_gradient-linear_trans code to help create
https://www.w3schools.com/css/tryit.asp?filename=trycss3_gradient-linear gradient code to help create
https://www.w3schools.com/css/css_howto.asp copy link to css 
https://www.w3schools.com/tags/tag_doctype.asp html structure
https://www.w3schools.com/css/css3_flexbox_container.asp flex box refrresher 
https://www.w3schools.com/tags/tag_link.asp copied links
https://stackoverflow.com/questions/46020589/html-not-linking-to-css-file-in-visual-studio-code help fix a connection issue between CSS and HTML
https://www.istockphoto.com/photo/smiling-barista-offering-coffee-to-pretty-customer-gm803473842-130292525 Stock Photo
https://codepen.io/erikterwan/pen/EVzeRP hamburger menu 
http://www.alexhonnold.com/about#bio inspiration for the about page
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_half_page copied code to make 1/2 page background hero image
https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_image_transparent_bottom used to make the photo caption's background rectangle
https://stackoverflow.com/questions/3443606/make-footer-stick-to-bottom-of-page-correctly copied code to make a sticky footer on about page
https://www.w3schools.com/howto/howto_css_image_text.asp copied code to place text over hero image on the about page
https://www.rapidtables.com/convert/color/hex-to-rgb.html used to convert my hex codes into RGB
https://www.w3schools.com/css/css_text_shadow.asp text shadow on 'John Doe'- About Page
https://stackoverflow.com/questions/31974448/how-can-i-prevent-having-just-one-hanging-word-on-a-new-line-in-an-html-element copied '&nbsp;'insert on html that helps prevent runts at the end of paragraphs
https://blog.hubspot.com/website/center-an-image-in-html copied code to position logo
https://css-tricks.com/snippets/css/a-guide-to-flexbox/ refresher on flexbox
http://www.corelangs.com/css/box/ontop.html copied code to help position images on about page
https://www.emailonacid.com/blog/article/email-development/emailology_media_queries_demystified_min-width_and_max-width/ used this code to inform me on the proper syntax for my media queries
https://www.w3schools.com/css/css3_mediaqueries.asp refresher on media queries 
https://www.w3schools.com/cssref/pr_text_letter-spacing.asp used to space letters in "Nice to meet you!" in about.html
https://bagelheads.com/about-us/ origianl site i based my project on- copied some text from the original site to provide paragraph content
https://getbootstrap.com/docs/5.0/components/navbar/ bootstrap used to insert navbar
https://css-tricks.com/snippets/css/a-guide-to-flexbox/#flexbox-tricks reference flex for menu set up
https://getbootstrap.com/docs/5.0/components/collapse/#accessibility copied 'collapse' bootstrap code to make the menu buttons with product descriptions

Menu Images downloaded from iStock:
https://www.istockphoto.com/photo/strawberry-muffin-gm104239633-1273061
https://www.istockphoto.com/photo/muffin-gm120290706-15873564
https://www.istockphoto.com/photo/muffin-with-apricot-gm472000119-30466630
https://www.istockphoto.com/photo/delicious-tasty-muffin-isolated-on-white-background-gm1224859860-360329247
https://www.istockphoto.com/photo/bagel-with-cream-cheese-figs-raspberries-blackberries-and-honey-gourmet-sandwich-on-gm1281680489-379650666
https://www.istockphoto.com/photo/cookie-ring-with-chopped-roasted-nuts-close-up-on-white-background-gm1182996383-332434018
https://www.istockphoto.com/photo/bagel-gm537716382-95473057
https://www.istockphoto.com/photo/sweet-sugar-cookies-closeup-isolated-gm1157284438-315723472
https://www.istockphoto.com/photo/bagel-with-red-fish-and-soft-cheese-isolated-top-view-gm475832092-65584201
https://www.istockphoto.com/photo/croissants-gm502179240-81754947
https://stackoverflow.com/questions/35397709/add-background-image-in-bootstrap-row copied background image style code for order page
https://codepen.io/pelinoloji/pen/GzWVZV copied order page checkout form from bootstrap code from codepen
https://www.istockphoto.com/vector/white-bagels-with-poppy-seamless-pattern-gm493450281-40335082 image from istock for order background
https://www.w3schools.com/howto/howto_css_contact_form.asp copied CSS Code and HTML code from this example and adjusted slightly to create contact form
https://lesscss.org/ Used to help inform my decisions on LESS-- though I had trouble finding an appropriate context for mixins


